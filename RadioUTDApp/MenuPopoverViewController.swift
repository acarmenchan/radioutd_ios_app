//
//  menuPopoverViewController.swift
//  RadioUTDApp
//
//  Created by Carmen Chan on 3/19/16.
//  Copyright © 2016 RadioUTD. All rights reserved.
//

import UIKit

class MenuPopoverViewController: UITableViewController {
    var menuIcons = [UIImage]()
    var menuTitles = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadMenuData()
    }
    
    func loadMenuData() {
        self.menuIcons.append(UIImage(named: "MenuSave_Dark")!)
        self.menuTitles.append("Add to Favorites")
        
        self.menuIcons.append(UIImage(named: "MenuFavorites_Dark")!)
        self.menuTitles.append("My Favorites")
        
        self.menuIcons.append(UIImage(named: "MenuShare_Dark")!)
        self.menuTitles.append("Share")
        
        self.menuIcons.append(UIImage(named: "MenuReminder_Dark")!)
        self.menuTitles.append("Set Show Reminder")
        
        self.menuIcons.append(UIImage(named: "MenuDarkMode_Dark")!)
        self.menuTitles.append("Dark Mode")
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuIcons.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MenuTableViewCell", forIndexPath: indexPath) as! MenuTableViewCell
        
        cell.menuItemIconButton.setImage(self.menuIcons[indexPath.row], forState: UIControlState.Normal)
        cell.menuItemTitle.text = self.menuTitles[indexPath.row]
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.contentView.backgroundColor = UIColor.init(red: 130/255, green: 130/255, blue: 130/255, alpha: 0.60)
    }
    
    override func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.contentView.backgroundColor = UIColor.clearColor()
    }
}