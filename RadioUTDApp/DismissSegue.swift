//
//  DismissSegue.swift
//  RadioUTDApp
//
//  Created by John Watson on 3/20/16.
//  Copyright © 2016 RadioUTD. All rights reserved.
//

import UIKit

class DismissSegue: UIStoryboardSegue {
    
    override func perform() {
        self.sourceViewController.presentingViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            //
        })
    }
}