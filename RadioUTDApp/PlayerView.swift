//
//  PlayerView.swift
//  RadioUTDApp
//
//  Created by Carmen Chan on 3/19/16.
//  Copyright © 2016 RadioUTD. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

protocol PlayerViewDelegate {
    func playerViewDidRequestInfoView()
}

@IBDesignable class PlayerView: UIView {
    
    let BUTTON_FADEOUT_DELAY: NSTimeInterval = 1.5
    let BUTTON_FULL_OPACITY: CGFloat = 0.80
    
    var isPlaying: Bool = false
    
    var radioPlayer: MPMoviePlayerController = MPMoviePlayerController()
    
    let playButtonName         = "PlayButton_Dark"
    let playButtonSelectedName = "PlayButton_Selected_Dark"
    
    let pauseButtonName         = "PauseButton_Dark"
    let pauseButtonSelectedName = "PauseButton_Selected_Dark"
    
    var playButtonImage:  UIImage
    var playButtonSelectedImage: UIImage
    var pauseButtonImage: UIImage
    var pauseButtonSelectedImage: UIImage
    
    var delegate: PlayerViewDelegate? = nil
    
    @IBOutlet weak var audioControlButton: UIButton!
    @IBOutlet weak var showIconImage: UIImageView!
    @IBOutlet weak var infoButton: UIButton!
    
    @IBOutlet var view: UIView!
    
    @IBAction func infoButtonPressed(sender: AnyObject) {
        self.delegate!.playerViewDidRequestInfoView()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "PlayerView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        
        return view
    }
    
    override init(frame: CGRect) {
        self.playButtonImage         = UIImage(named: self.playButtonName, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
        self.playButtonSelectedImage = UIImage(named: self.playButtonSelectedName, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
        
        self.pauseButtonImage         = UIImage(named: self.pauseButtonName, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
        self.pauseButtonSelectedImage = UIImage(named: self.pauseButtonSelectedName, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
        
        super.init(frame: frame)
        
        self.xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        self.playButtonImage         = UIImage(named: self.playButtonName, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
        self.playButtonSelectedImage = UIImage(named: self.playButtonSelectedName, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
        
        self.pauseButtonImage         = UIImage(named: self.pauseButtonName, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
        self.pauseButtonSelectedImage = UIImage(named: self.pauseButtonSelectedName, inBundle: NSBundle(forClass: self.dynamicType), compatibleWithTraitCollection: nil)!
        
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    override func awakeFromNib() {
        print("I'm in awake from Nib!")
        
        self.playButtonImage          = UIImage(named: self.playButtonName)!
        self.playButtonSelectedImage  = UIImage(named: self.playButtonSelectedName)!
        self.pauseButtonImage         = UIImage(named: self.pauseButtonName)!
        self.pauseButtonSelectedImage = UIImage(named: self.pauseButtonSelectedName)!
    }
    
    override func didMoveToSuperview() {
        self.audioControlButton.alpha = self.BUTTON_FULL_OPACITY
        self.setPlayButtonImage()
        
        print("I'm in PlayerView!")
        
        self.setUpPlayer()
        
        radioPlayer.contentURL = NSURL(string: "http://ghost.wavestreamer.com:3203/")
    }
    func setUpPlayer() {

        radioPlayer.view.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        radioPlayer.view.sizeToFit()
        radioPlayer.movieSourceType = MPMovieSourceType.Streaming
        radioPlayer.fullscreen = false
        
        radioPlayer.controlStyle = MPMovieControlStyle.None
        radioPlayer.contentURL = NSURL(string: "http://ghost.wavestreamer.com:3203/")
        radioPlayer.prepareToPlay()
//        radioPlayer.pause()
    }

    @IBAction func playPauseButtonPressed(sender: AnyObject) {
        if self.isPlaying
        {
            //PLAY STATE
            //if playing TURN MUSIC OFF!
            self.radioPlayer.pause()
            self.isPlaying = false

            //show them the play button so they can turn the music back ON
            self.setPlayButtonImage()
            self.showIconImage.userInteractionEnabled = false
        }
        else
        {
            //PAUSE STATE
            //if not playing TURN MUSIC ON!
//            self.audioPlayer!.prepareToPlay()
            radioPlayer.play()

            self.isPlaying = true

            //show them the pause button so they can turn the music back OFF
            self.setPauseButtonImage()
            self.hidePlayPauseButtonWithDelay(BUTTON_FADEOUT_DELAY)
            self.showIconImage.userInteractionEnabled = true
        }
    }

    @IBAction func handleShowIconTap(recognizer: UITapGestureRecognizer) {
        print("TAPPED THE SHOW ICON")
        if self.audioControlButton.alpha > 0 {     //if button is visible, fade out
            hidePlayPauseButtonWithDelay(0)
        }
        else {      //if button is hidden, fade in
            self.showPlayPauseButton()
        }
    }

    func setPlayButtonImage() {
        self.audioControlButton.setImage(self.playButtonImage, forState: .Normal)
        self.audioControlButton.setImage(self.playButtonSelectedImage, forState: .Highlighted)
    }

    func setPauseButtonImage() {
        self.audioControlButton.setImage(self.pauseButtonImage, forState: .Normal)
        self.audioControlButton.setImage(self.pauseButtonSelectedImage, forState: .Highlighted)
    }

    func hidePlayPauseButtonWithDelay(delay:NSTimeInterval) {
        UIView.animateWithDuration(0.5,
            delay: delay,
            options: UIViewAnimationOptions.CurveLinear,
            animations: {
                self.audioControlButton.alpha = 0
            },
            completion: nil)
    }

    func showPlayPauseButton() {
        UIView.animateWithDuration(0.5,
            animations: {
                self.audioControlButton.alpha = self.BUTTON_FULL_OPACITY
            },
            completion: nil)
    }
    
}