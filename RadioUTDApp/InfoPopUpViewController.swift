//
//  InfoPopUpViewController.swift
//  RadioUTDApp
//
//  Created by Carmen Chan on 3/19/16.
//  Copyright © 2016 RadioUTD. All rights reserved.
//

import UIKit

class InfoPopUpViewController: UIViewController {

    @IBOutlet weak var showTitleLabel: UILabel!
    @IBOutlet weak var djNameLabel: UILabel!
    @IBOutlet weak var showTimeLabel: UILabel!
    @IBOutlet weak var showDescriptionLabel: UILabel!
    @IBOutlet weak var showRiylLabel: UILabel!
    
    @IBOutlet weak var seeLatestPlaylistButton: UIButton!
    
    let seeLatestPlaylistWeblinkString = "https://www.radioutd.com/shows/?id=2135"
    
    @IBAction func seeLatestPlaylistButtonPressed(sender: AnyObject) {
        print("See Latest Playlist button pressed")
        let url = NSURL(string: seeLatestPlaylistWeblinkString)
        UIApplication.sharedApplication().openURL(url!)
    }
    
    override func viewWillDisappear(animated: Bool) {
        let vc = self.presentingViewController as! ViewController
        
        vc.isShowingInfoView = false
        self.presentingViewController?.setNeedsStatusBarAppearanceUpdate()
    }
}