//
//  ViewController.swift
//  RadioUTDApp
//
//  Created by Carmen Chan on 3/15/16.
//  Copyright © 2016 RadioUTD. All rights reserved.
//

import UIKit

enum SocialMediaType {
    case Facebook
    case Twitter
    case Instagram
}

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate, PlayerViewDelegate {
    
    let radioUTDFacebookDeeplinkString  = "fb://profile?id=RadioUTD"
    let radioUTDTwitterDeeplinkString   = "twitter://user?screen_name=RadioUTD"
    let radioUTDInstagramDeeplinkString = "instagram://user?username=radioutd"
    
    let radioUTDFacebookWeblinkString   = "https://www.facebook.com/RadioUTD"
    let radioUTDTwitterWeblinkString    = "https://twitter.com/RadioUTD"
    let radioUTDInstagramWeblinkString  = "https://www.instagram.com/radioutd/"
    
    @IBOutlet weak var playerView: PlayerView!
    @IBOutlet weak var menuButton: UIButton!
    
    var isShowingInfoView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.playerView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //social media button handlers
    @IBAction func facebookButtonPressed(sender: AnyObject) {
        print("Facebook button pressed")
        self.openMediaLink(.Facebook)
    }
    
    @IBAction func twitterButtonPressed(sender: AnyObject) {
        print("Twitter button pressed")
        self.openMediaLink(.Twitter)
    }
    
    @IBAction func instagramButtonPressed(sender: AnyObject) {
        print("Instagram button pressed")
        self.openMediaLink(.Instagram)
    }
    
    func openMediaLink(socialMediaType: SocialMediaType) {
        let deeplinkString: String
        let weblinkString: String
        
        switch socialMediaType {
        case .Facebook:
            deeplinkString = self.radioUTDFacebookDeeplinkString
            weblinkString  = self.radioUTDFacebookWeblinkString
            
        case .Twitter:
            deeplinkString = self.radioUTDTwitterDeeplinkString
            weblinkString  = self.radioUTDTwitterWeblinkString
            
        case .Instagram:
            deeplinkString = self.radioUTDInstagramDeeplinkString
            weblinkString  = self.radioUTDInstagramWeblinkString
        }
        
        let deeplinkURL = NSURL(string: deeplinkString)
        let weblinkURL  = NSURL(string: weblinkString)
        if UIApplication.sharedApplication().canOpenURL(deeplinkURL!) {
            UIApplication.sharedApplication().openURL(deeplinkURL!)
            
        } else {
            //redirect to weblink in safari if user doesn't have targeted app installed
            UIApplication.sharedApplication().openURL(weblinkURL!)
        }
    }
    
    //menu button
    @IBAction func menuButtonPressed(sender: AnyObject) {
        print("Menu Button Pressed")
        self.performSegueWithIdentifier("menuSegue", sender: self)
    }
    
    func playerViewDidRequestInfoView() {
        print("Info Button Pressed")
        self.performSegueWithIdentifier("displayInfoViewSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "menuSegue" {
            
            let vc = segue.destinationViewController
            vc.preferredContentSize = CGSizeMake(250, 229)
            
            let controller = vc.popoverPresentationController
            
            if controller != nil {
                controller?.delegate = self
                controller?.backgroundColor = UIColor.init(colorLiteralRed: 30/255, green: 30/255, blue: 30/255, alpha: 0.60)
                controller?.sourceRect = menuButton.bounds
            }
        }
        if segue.identifier == "displayInfoViewSegue" {
            self.isShowingInfoView = true
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        if self.isShowingInfoView {
            return UIStatusBarStyle.LightContent
        }
        else {
            return UIStatusBarStyle.Default
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
}