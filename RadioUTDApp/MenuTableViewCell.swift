//
//  menuCell.swift
//  RadioUTDApp
//
//  Created by Carmen Chan on 3/19/16.
//  Copyright © 2016 RadioUTD. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuItemTitle: UILabel!
    @IBOutlet weak var menuItemIconButton: UIButton!

}
